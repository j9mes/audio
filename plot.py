import matplotlib.pyplot as plt
from numpy import genfromtxt

freq_data = genfromtxt('frequency_data.csv', delimiter=',')
plt.plot(freq_data)

plt.title('Freq detected over time')
plt.ylabel('Freq')
plt.xlabel('time')

plt.show()
